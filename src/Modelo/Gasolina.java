
package Modelo;

public class Gasolina {
    private int id;
    private String tipo;
    private float precio;

    public Gasolina(){
    }

    public Gasolina(int id, String tipo, float precio) {
        this.id = id;
        this.tipo = tipo;
        this.precio = precio;
    }

    public Gasolina(Gasolina Gas){
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
        System.out.println(precio);
    }

    public int getId() {
        return id;
    }

    public String getTipo() {
        return tipo;
    }

    public float getPrecio() {
        return precio;
    }
    
    
    
}
