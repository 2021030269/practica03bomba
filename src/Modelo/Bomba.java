
package Modelo;

public class Bomba {
    private int numBom;
    private Gasolina gas;
    private float capaBom,acumLts;

    public Bomba() {
    }

    public Bomba(int numBom, Gasolina gas, float capaBom, float acumLts) {
        this.numBom = numBom;
        this.gas = gas;
        this.capaBom = capaBom;
        this.acumLts = acumLts;
    }

    public Bomba(Bomba bom) {
    }

    public void setNumBom(int numBom) {
        this.numBom = numBom;
    }

    public void setGas(Gasolina gas) {
        this.gas = gas;
    }

    public void setCapaBom(float capaBom) {
        this.capaBom = capaBom;
    }

    public void setAcumLts(float acumLts) {
        this.acumLts += acumLts;
    }

    public int getNumBom() {
        return numBom;
    }

    public Gasolina getGas() {
        return gas;
    }

    public float getCapaBom() {
        return capaBom;
    }

    public float getAcumLts() {
        return acumLts;
    }
    
    public void iniciarBomba(int numbom,Gasolina gas, float capaBom, float acumLts){
        setNumBom(numbom);
        setGas(gas);
        setCapaBom(capaBom);
        setAcumLts(acumLts);
    }
    
    public float inventarioGasolina(){
        return getCapaBom()-getAcumLts();
    }
    
    public float venderGasolina(float cant){
        if(inventarioGasolina()>=cant){
            setAcumLts(cant);
            return cant*getGas().getPrecio();
        }
        else{
            return 0;
        }
    }
    
    public float ventasTotales(){
        return getAcumLts()*getGas().getPrecio();
    } 
    
    
    
}
