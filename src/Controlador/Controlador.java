
package Controlador;

import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.dlgBomba;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private float vent;
    private int cont=1;
    private Bomba bom;
    private dlgBomba vista;

    public Controlador(Bomba bom, dlgBomba vista) {
        this.bom = bom;
        this.vista = vista;
        
        vista.btnIniBom.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
    }
    
    private void iniciarVista(){
        vista.setTitle(":: BOMBA ::");
        vista.setSize(1026,779);
        vista.setVisible(true);

    }
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnIniBom){
            
            
            
            if(ValiEspIniBom() == true){
                float p;
                p = Precio();
                try{
                    bom.iniciarBomba(Integer.parseInt(vista.txtNumBom.getText()),new Gasolina(vista.cboTipoGasolina.getSelectedIndex(),vista.cboTipoGasolina.getSelectedItem().toString(),p),vista.jSCapaBom.getValue(),0);
                    vista.txtPrecioVenta.setText(String.valueOf(bom.getGas().getPrecio()));
                    vista.txtNumBom.setEnabled(false);
                    vista.cboTipoGasolina.setEnabled(false);
                    vista.jSCapaBom.setEnabled(false);
                    vista.txtCantidad.setEnabled(true);
                    vista.btnRegistrar.setEnabled(true);
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                    
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }
            
        }
        else if(e.getSource() == vista.btnRegistrar){
            if(ValiEspRegistro() == true){
                try{
                    vent = bom.venderGasolina(Float.parseFloat(vista.txtCantidad.getText()));
                    if(vent != 0){
                        vista.txtCosto.setText(String.valueOf(vent));
                        vista.txtContador.setText(String.valueOf(cont++));
                        vista.jSCapaBom.setValue((int)(bom.getCapaBom() - bom.getAcumLts()));
                        vista.txtTotalVentas.setText(String.valueOf(bom.ventasTotales()));
                    }
                    else{
                        JOptionPane.showMessageDialog(vista,"La cantidad que pide supera a la capacidad de la Bomba");
                    }
                    
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                    
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
                
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }
            
            
        }
        else if(e.getSource()==vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(vista,"Seguro que desea cerrar?","Cerrar",JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION){
                
            }
            else{
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }   
        }
        
    }
    public float Precio(){
        if(vista.cboTipoGasolina.getSelectedIndex() == 1){
            return 22.17f;
        }
        else if(vista.cboTipoGasolina.getSelectedIndex() == 2){
            return 23.98f;
        }
        else if(vista.cboTipoGasolina.getSelectedIndex() == 3){
            return 24.71f;
        }
        else{
            return 0f;
        }
    }
    
    public boolean ValiEspRegistro(){
        if(!vista.txtCantidad.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean ValiEspIniBom(){
        if(!vista.txtNumBom.getText().isEmpty()
            && vista.cboTipoGasolina.getSelectedItem() != " "
            && vista.jSCapaBom.getValue() != 0){
            return true;
        
        }
        else{
            return false;
        }
    }
    
   
    
    public static void main(String[] args) {
        Bomba bom = new Bomba();
        dlgBomba vista = new dlgBomba();
        Controlador contr = new Controlador(bom, vista);
        contr.iniciarVista();

    }
}
